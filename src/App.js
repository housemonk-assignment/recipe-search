/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-useless-concat */
import {  useState } from "react";
import "./App.css";
import SearchRecipe from './Component/SearchRecipe/SearchRecipe';
import SingleRecipe from "./Component/SingleRecipe/SingleRecipe";

function App() {
  const [showSingle, setShowSingle] = useState(false)
  const [show, setShow] = useState(false)
  const [idr,setId] = useState(0)

  function handleShowSingle(id){
    setShowSingle(true)
    setShow(true)
    setId(id)
  }
  
  return <div>{!showSingle && <SearchRecipe show={handleShowSingle}/>}
  {show && <SingleRecipe id={idr}/>}</div>;
}

export default App;
