/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-useless-concat */
import "./SearchRecipe.css";
import axios from "axios";
import React, { useEffect, useState } from "react";


function SearchRecipe(props) {
  const [post, setPost] = useState(null);
  const [idr, setId] = useState("abc");
  useEffect(() => {
    console.log("re-render because x changed:", idr);
    if(idr!=='abc'){
        props.show(idr)
    }
  }, [idr]);
  function handleKeyUp(event) {
    if (event.target.value === null) {
      setPost(null);
    } else {
      axios
        .get(
          `${process.env.REACT_APP_URL1}` +
            "q=" +
            event.target.value +
            "&app_id=" +
            `${process.env.REACT_APP_API_ID}` +
            "&app_key=" +
            `${process.env.REACT_APP_API_KEY}`
        )
        .then((response) => {
          setPost(response.data);
          console.log(post);
        });
    }
  }
  function handleRecipeClick(id) {
    setId(id.substring(44, id.length));
  }
  function checkPost() {
    if (!post) {
      return (
        <div>
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <form class="example" action="/action_page.php">
              <input
                type="text"
                placeholder="Search.."
                name="search"
                onChange={handleKeyUp}
              />
            </form>
          </nav>
        </div>
      );
    }
    else {
      return (
        <div>
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <form class="example" action="/action_page.php">
              <input
                type="text"
                placeholder="Search.."
                name="search"
                onChange={handleKeyUp}
              />
            </form>
          </nav>
          <hr></hr>
          <nav class="navbar navbar-expand-lg navbar-light bg-light"></nav>
          <div class="content">
            <div class="container">
              <div class="row">
                {post.hits.map((food) => (
                  <div class="col-xs-12 col-sm-4">
                    <div
                      class="card"
                      onClick={() => handleRecipeClick(food.recipe.uri)}
                    >
                      <p class="img-card">
                        <img src={food.recipe.image} />
                      </p>
                      <div class="card-content">
                        <h4 class="card-title">
                          <p> {food.recipe.label}</p>
                        </h4>
                        <hr></hr>
                        <p class="">
                          <span>
                            {Math.round(food.recipe.calories)} CALORIES
                          </span>{" "}
                          |{" "}
                          <span>
                            {food.recipe.ingredientLines.length} INGREDIENTS
                          </span>
                        </p>
                        <hr></hr>
                      </div>
                      <div class="card-read-more">
                        <p class="margin">{food.recipe.source}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  return <div>{checkPost()}</div>;
}

export default SearchRecipe;
