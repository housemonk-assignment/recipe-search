/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-useless-concat */
import "./SingleRecipe.css";
import axios from "axios";
import React, { useEffect, useState } from "react";

function SingleRecipe(props) {
  const [post, setPost] = useState(null);
  useEffect(() => {
    axios
      .get(
        `${process.env.REACT_APP_URL2}` +
          props.id +
          "?type=public" +
          "&app_id=" +
          `${process.env.REACT_APP_API_ID}` +
          "&app_key=" +
          `${process.env.REACT_APP_API_KEY}`
      )
      .then((response) => {
        setPost(response.data);
        console.log(post);
      });
  }, []);

  return (
    <div class="container">
      <div class="jumbotron">
        {/* <h1 class="display-4">Hello, world!</h1> */}
        <div class="flex">
        <img src={(post===null)?null:post.recipe.image}></img>
        <h1 class="lead">
          {(post===null)?null:post.recipe.label}<br></br>
          <span class="nel">See full food on:<u>{(post===null)?null:post.recipe.source}</u></span>
        </h1>
        </div>
        <hr class="my-4" />
        <div class="flex1">
          <ul class="list-ws">
          <h4>{(post===null)?null:post.recipe.ingredientLines.length} Ingredients</h4>
          {(post===null)?null:post.recipe.ingredientLines.map((ingredient)=>(
            <li>{ingredient}</li>
          ))}
          </ul>
          <ul class="list-ws">
          <h4>Nutrition</h4>
          <div class="flex1">
            <li class="wi">
              {(post===null)?null:Math.round(post.recipe.calories)}<br></br>
              <span>CAL/SER</span>
            </li>
            <li class="wi">6%<br></br>
              <span>DAILY</span>
            </li>
            <li class="wi">4<br></br>
              <span>SERVINGS</span>
            </li>
          </div>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default SingleRecipe;
